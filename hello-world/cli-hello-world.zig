// run with:
// $ zig build-exe cli-hello-world.zig -lc
// $ ./cli-hello-world

const print = @import("std").debug.print;
const c = @cImport({
    @cInclude("stdio.h");
});

pub fn main() void {
    print("hello {s}\n", .{"world!"});
    _ = c.printf("hello world from c!\n");
}

